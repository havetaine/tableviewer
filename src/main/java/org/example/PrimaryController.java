package org.example;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;


public class PrimaryController
{
    @FXML
    private TextField urlField;
    @FXML
    private TextField portField;
    @FXML
    private TextField dbnameField;
    @FXML
    private TextField userNameField;
    @FXML
    private PasswordField passwordField;

    public void switchScenes(ActionEvent event) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("secondary.fxml"));
        Parent root = loader.load();
        System.out.println(loader.getLocation());
        SecondaryController secondary = loader.getController();
        secondary.getDbLoginInfo(urlField.getText(), portField.getText(), dbnameField.getText(),
                userNameField.getText(), passwordField.getText());

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
    }
}
