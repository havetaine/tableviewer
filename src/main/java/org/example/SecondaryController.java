package org.example;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import org.example.jdbc.Query;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SecondaryController
{
    @FXML
    private TableView<List<String>> resultTable;
    @FXML
    private TextArea queryField;
    @FXML
    private ListView<String> metadata;

    private String url;
    private String port;
    private String dbname;
    private String user;
    private String pass;

    public void getDbLoginInfo(String url, String port, String dbname, String user, String pass)
    {
        this.url = url;
        this.port = port;
        this.dbname = dbname;
        this.user = user;
        this.pass = pass;
    }

    public void getData(ActionEvent event)
    {
        if (queryField.getText().isEmpty())
        {
            return;
        }

        resultTable.getItems().clear();
        resultTable.getColumns().clear();

        try (Query query = new Query(url, port, user, dbname, pass))
        {
            query.queryDatabase(queryField.getText());
            makeTable(query.getResult());
            getMetadata(query.getConnect().connection);
        }
        catch (java.sql.SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void makeTable(ResultSet rs) throws SQLException
    {
        ResultSetMetaData meta = rs.getMetaData();
        List<List<String>> list = new ArrayList<>();

        while (rs.next())
        {
            List<String> data = new ArrayList<>();
            for (int i = 0; i < meta.getColumnCount(); i++)
            {
                // Retrieves one cell from each column.
                data.add(rs.getString(i + 1));
            }

            // Adds data list (one row) to list.
            list.add(data);
        }

        for (int i = 0; i < meta.getColumnCount(); i++)
        {
            final int index = i;
            TableColumn<List<String>, String> column = new TableColumn<>(meta.getColumnName(i + 1));
            rs.next();
            column.setCellValueFactory(data ->
            {
                List<String> tmp = data.getValue();
                return new ReadOnlyStringWrapper(tmp.get(index));
            });

            resultTable.getColumns().add(column);
        }

        for (List<String> tmp : list)
        {
            resultTable.getItems().add(tmp);
        }
    }

    private void getMetadata(Connection connection) throws SQLException
    {
        DatabaseMetaData metaData = connection.getMetaData();

        metadata.getItems().add("Baza danych: " + metaData.getDatabaseProductName());
        metadata.getItems().add("Wersja: " + metaData.getDatabaseProductVersion());

    }
}