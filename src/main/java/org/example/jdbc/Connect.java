package org.example.jdbc;

import java.sql.*;

public class Connect implements AutoCloseable
{
    private String host;
    private String port;
    private String dbname;
    private String user;
    private String url;
    private String pass;
    public Connection connection;

    private Connect(String host, String dbname, String user, String pass)
    {
        this.host = host;
        this.port = "5432";
        this.dbname = dbname;
        this.user = user;
        this.url = "jdbc:postgresql://" + host + ":5432" + "/" + dbname;
        this.pass = pass;
    }

    private Connect(String host, String port, String dbname, String user, String pass)
    {
        this.host = host;
        this.port = port;
        this.dbname = dbname;
        this.user = user;
        this.url = "jdbc:postgresql://" + host + ":" + port + "/" + dbname;
        this.pass = pass;
    }

    public static Connect getConnect(String host, String dbname, String user, String pass) throws SQLException
    {
        Connect connect = new Connect(host, dbname, user, pass);
        connect.connection = connect.makeConnection();

        return connect;
    }

    public static Connect getConnect(String host, String port, String dbname, String user, String pass) throws SQLException
    {
        Connect connect = new Connect(host, port, dbname, user, pass);
        connect.connection = connect.makeConnection();

        return connect;
    }

    private Connection makeConnection() throws SQLException
    {
        return DriverManager.getConnection(url, user, pass);
    }

    @Override
    public void close()
    {
        try
        {
            connection.close();
        }
        catch (SQLException e)
        {
            System.err.println("Connection cannot be closed: " + e);
            e.printStackTrace();
        }
    }

}
