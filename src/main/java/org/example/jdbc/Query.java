package org.example.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Query implements AutoCloseable
{
    private Connect connect;
    private Statement statement;
    private ResultSet result;

    public Query(String url, String port, String dbname, String user, String pass) throws SQLException
    {
        this.connect = Connect.getConnect(url, port, user, dbname, pass);
    }

    public void queryDatabase(String query) throws SQLException
    {
        this.statement = connect.connection.createStatement();
        this.result = statement.executeQuery(query);
    }

    @Override
    public void close()
    {
        try
        {
            connect.close();
            statement.close();
            result.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public Connect getConnect()
    {
        return connect;
    }

    public void setConnect(Connect connect)
    {
        this.connect = connect;
    }

    public Statement getStatement()
    {
        return statement;
    }

    public void setStatement(Statement statement)
    {
        this.statement = statement;
    }

    public ResultSet getResult()
    {
        return result;
    }

    public void setResult(ResultSet result)
    {
        this.result = result;
    }
}
