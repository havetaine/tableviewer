# tableViewer 🗄️
**tableViewer** is a program made with Java and JavaFX platform to perform simple SQL queries. It uses PostgreSQL JDBC Driver to connect to a database.  

## Interface
### Opening window
User can enter data that is necessary to connect to a PostgreSQL database:
- database address,
- port, 
- database name,
- user name,
- password.

![](images/login_window.jpg)  

### Results
It contains datatable at the top with the results and an input area at the bottom, where user can write SQL query.  
![](images/table_view.jpg)  
 

## Technology stack
- Java 17
- JavaFX 17.0.1
- PostgreSQL JDBC Driver 42.3.3